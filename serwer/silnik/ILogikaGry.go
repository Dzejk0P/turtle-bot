package silnik

import (
	"gitlab.com/Dzejk0P/turtle-bot/proto"
	"gitlab.com/Dzejk0P/turtle-bot/turtles"
)

type ILogikaGry interface {
	GetGameStatus(playerNumber int) (*proto.StanGry, error)
	Move(playerColor proto.KolorZolwia, cardSymbol proto.Karta, playerNumber int) error
}

func getLogikaGry(liczbaGraczy int) ILogikaGry {
	return turtles.CreateNewGame(liczbaGraczy)
}
