package boty

import (
	"log"
	"math"

	"gitlab.com/Dzejk0P/turtle-bot/klient/mysrv"
	"gitlab.com/Dzejk0P/turtle-bot/proto"
)

type BotV2 struct {
	Kolor       proto.KolorZolwia
	Przeciwnicy []proto.KolorZolwia
}

func NowyBotV2(stan *proto.StanGry) Bot {
	zolwie := []proto.KolorZolwia{
		proto.KolorZolwia_RED,
		proto.KolorZolwia_GREEN,
		proto.KolorZolwia_BLUE,
		proto.KolorZolwia_YELLOW,
		proto.KolorZolwia_PURPLE}

	bot := &BotV2{
		Kolor:       stan.TwojKolor,
		Przeciwnicy: wybierzPrzeciwnikow(zolwie, stan.TwojKolor),
	}

	return bot
}

func (bot *BotV2) WybierzKarte(stan *proto.StanGry) (proto.Karta, proto.KolorZolwia) {
	maxScore := -100.0
	var kartaa proto.Karta
	var kolorr proto.KolorZolwia

	for _, karta := range stan.TwojeKarty {
		polaCopy := copyPola(stan.Plansza)
		kolory := pobierzDostepneKolory(polaCopy, karta)
		for _, kolor := range kolory {
			polaCopy := copyPola(stan.Plansza)
			// fmt.Printf("karta: %s\n", karta)
			// fmt.Printf("kolor: %v\n", kolor)
			pola := mysrv.ZagrajKarte(polaCopy, karta, kolor)
			if bot.czyWygrywam(pola) {
				return karta, kolor
			}
			wynik := bot.policzWynik(pola)
			// fmt.Printf("wynik: %v\n", wynik)
			// b, _ := json.Marshal(pola)
			// fmt.Println(string(b))
			// fmt.Println()
			if wynik > maxScore {
				maxScore = wynik
				kolorr = kolor
				kartaa = karta
			}
		}

	}

	return kartaa, kolorr
}

func (bot *BotV2) czyWygrywam(pola []proto.Pole) bool {
	zolwie := pola[len(pola)-1].Zolwie
	if len(zolwie) == 0 {
		return false
	}
	return zolwie[len(zolwie)-1] == bot.Kolor

}

func (bot *BotV2) policzWynik(pola []proto.Pole) float64 {
	pole, nad, pod := bot.mojePolozenie(pola)
	przeciwnik := bot.najdalszyPrzeciwnik(pola)
	poleNorm := normalizuj(pole, 0, 10)
	// fmt.Printf("poleNorm: %v\n", poleNorm)
	przeciwnikNorm := normalizuj(przeciwnik, 0, 10)
	// fmt.Printf("przeciwnikNorm: %v\n", przeciwnikNorm)
	nadNorm := normalizuj(nad, 0, 4)
	// fmt.Printf("nadNorm: %v\n", nadNorm)
	podNorm := normalizuj(pod, 0, 4)
	// fmt.Printf("podNorm: %v\n", podNorm)

	modyfikator := 0.0
	// if bot.czyPrzeciwnikMozeWygrac(pola) {
	// 	modyfikator += 0.2
	// }

	return poleNorm - math.Sqrt(przeciwnikNorm)*0.7 - nadNorm*0.5 + podNorm*0.35 + modyfikator
}

func (bot *BotV2) mojePolozenie(pola []proto.Pole) (int, int, int) {
	for i, pole := range pola {
		for j, zolw := range pole.Zolwie {
			if zolw == bot.Kolor {
				if len(pole.Zolwie) == 1 {
					return i + 1, 0, 0
				}
				if j == len(pole.Zolwie)-1 {
					return i + 1, 0, len(pole.Zolwie) - 1
				}
				return i + 1, len(pole.Zolwie[j+1:]), len(pole.Zolwie[0:j])
			}
		}
	}
	return 0, 0, 0
}

func (bot *BotV2) najdalszyPrzeciwnik(pola []proto.Pole) int {
	for i := len(pola) - 1; i >= 0; i-- {
		pole := pola[i]
		for _, zolw := range pole.Zolwie {
			if zolw != bot.Kolor {
				return i + 1
			}
		}
	}
	return 0
}

func (bot *BotV2) wyliczMojaPozycje(pola []proto.Pole) int {
	licznik := 0
	for i := len(pola) - 1; i >= 0; i-- {
		for _, zolw := range pola[i].Zolwie {
			licznik++
			if zolw == bot.Kolor {
				return licznik
			}
		}
	}
	return licznik
}

func (bot *BotV2) czyPrawieWygram(pola []proto.Pole) bool {
	pozycja := bot.wyliczMojaPozycje(pola)
	if pozycja != 2 {
		return false
	}

	if len(pola[len(pola)-1].Zolwie) > 0 {
		log.Println("Czy prawie wygram: tak")
		return true
	}

	return false
}

func (bot *BotV2) czyPrzeciwnikMozeWygrac(pola []proto.Pole) bool {
	zolwie := pola[len(pola)-2].Zolwie

	for i := len(zolwie) - 1; i >= 0; i-- {
		if zolwie[i] != bot.Kolor {
			return true
		}
	}
	return false
}

// var RuchyDoTylu = map[proto.KolorZolwia][]proto.Karta{
// 	proto.KolorZolwia_RED: []proto.Karta{31, 32},
// 	proto.KolorZolwia_YELLOW: []proto.Karta{33, 34},
// 	proto.KolorZolwia_GREEN: []proto.Karta{35, 36},
// 	proto.KolorZolwia_BLUE: []proto.Karta{37, 38},
// 	proto.KolorZolwia_PURPLE: []proto.Karta{39, 40},
// }
