package boty

import (
	"fmt"
	"log"
	"strings"

	"gitlab.com/Dzejk0P/turtle-bot/proto"
)

type Czlowiek struct {
}

func NowyCzlowiek(stan *proto.StanGry) Bot {
	return &Czlowiek{}
}

func (c *Czlowiek) WybierzKarte(stan *proto.StanGry) (proto.Karta, proto.KolorZolwia) {
	karta := wczytajKarte()
	kolor := proto.KolorZolwia_XXX

	if _, ok := kartyDlaKtorychTrzebaPodacKolor[karta]; ok {
		kolor = wczytajKolor()
	}

	return karta, kolor
}

func wczytajKolor() proto.KolorZolwia {
	fmt.Print("Wybierz kolor\n> ")
	var kolor string
	_, err := fmt.Scanln(&kolor)
	if err != nil {
		log.Fatalf("Błąd wczytywania koloru: %v", err)
	}
	k, ok := proto.KolorZolwia_value[strings.ToUpper(kolor)]
	if !ok {
		log.Fatalf("Niepoprawny kolor: %q", kolor)
	}
	return proto.KolorZolwia(k)
}

func wczytajKarte() proto.Karta {
	var karta proto.Karta
	for {
		fmt.Print("Wybierz kartę do zagrania:\n> ")
		var kartatxt string
		_, err := fmt.Scanln(&kartatxt)
		if err != nil {
			log.Fatalf("Błąd wczytywania karty: %v", err)
		}
		k, ok := proto.Karta_value[strings.ToUpper(kartatxt)]
		if !ok {
			fmt.Printf("Niepoprawna karta: %q\n", kartatxt)
			continue
		}
		karta = proto.Karta(k)
		break
	}
	return karta
}
