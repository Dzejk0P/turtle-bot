package boty

import (
	"math/rand"

	"gitlab.com/Dzejk0P/turtle-bot/proto"
)

type BotLosowy struct {
	Kolor       proto.KolorZolwia
	Przeciwnicy []proto.KolorZolwia
}

func NowyBotLosowy(stan *proto.StanGry) Bot {
	zolwie := []proto.KolorZolwia{
		proto.KolorZolwia_RED,
		proto.KolorZolwia_GREEN,
		proto.KolorZolwia_BLUE,
		proto.KolorZolwia_YELLOW,
		proto.KolorZolwia_PURPLE}

	bot := &BotLosowy{
		Kolor:       stan.TwojKolor,
		Przeciwnicy: wybierzPrzeciwnikow(zolwie, stan.TwojKolor),
	}

	return bot
}

func (bot *BotLosowy) WybierzKarte(stan *proto.StanGry) (proto.Karta, proto.KolorZolwia) {
	losulosu := rand.Intn(len(stan.TwojeKarty))
	losowaKarta := stan.TwojeKarty[losulosu]
	kolor := proto.KolorZolwia_XXX

	if _, ok := kartyDlaKtorychTrzebaPodacKolor[losowaKarta]; ok {
		kolor = bot.wybierzKolor(stan, losowaKarta)
	}

	return losowaKarta, kolor
}

func (bot *BotLosowy) wybierzKolor(stan *proto.StanGry, karta proto.Karta) proto.KolorZolwia {
	ostatnie := ostatnieZolwie(copyPola(stan.Plansza))
	switch karta {
	case dowolnyDoPrzodu:
		return bot.Kolor
	case dowolnyDoTylu:
		//randomowy przeciwnik
		return bot.Przeciwnicy[rand.Intn(len(bot.Przeciwnicy))]
	case ostatniDoPrzodu, ostatniDwaDoPrzodu:
		return ostatnie[rand.Intn(len(ostatnie))]
	}
	return proto.KolorZolwia_XXX
}

// var RuchyDoTylu = map[proto.KolorZolwia][]proto.Karta{
// 	proto.KolorZolwia_RED: []proto.Karta{31, 32},
// 	proto.KolorZolwia_YELLOW: []proto.Karta{33, 34},
// 	proto.KolorZolwia_GREEN: []proto.Karta{35, 36},
// 	proto.KolorZolwia_BLUE: []proto.Karta{37, 38},
// 	proto.KolorZolwia_PURPLE: []proto.Karta{39, 40},
// }
