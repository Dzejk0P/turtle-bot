package boty

import (
	"slices"

	"gitlab.com/Dzejk0P/turtle-bot/proto"
)

var dowolnyDoPrzodu = proto.Karta_A1
var dowolnyDoTylu = proto.Karta_A1B

var ostatniDoPrzodu = proto.Karta_L1
var ostatniDwaDoPrzodu = proto.Karta_L2

var kartyDlaKtorychTrzebaPodacKolor = map[proto.Karta]bool{
	proto.Karta_L1:  true,
	proto.Karta_L2:  true,
	proto.Karta_A1:  true,
	proto.Karta_A1B: true,
}

var zolwie = []proto.KolorZolwia{
	proto.KolorZolwia_RED,
	proto.KolorZolwia_GREEN,
	proto.KolorZolwia_BLUE,
	proto.KolorZolwia_YELLOW,
	proto.KolorZolwia_PURPLE,
}

type Bot interface {
	WybierzKarte(stan *proto.StanGry) (proto.Karta, proto.KolorZolwia)
}

func wybierzPrzeciwnikow(wszyscy []proto.KolorZolwia, my proto.KolorZolwia) []proto.KolorZolwia {
	for i, other := range wszyscy {
		if other == my {
			return append(wszyscy[:i], wszyscy[i+1:]...)
		}
	}
	return wszyscy
}

// func ostatnieZolwie(pola []proto.Pole) []proto.KolorZolwia {
// 	wystapienia := map[proto.KolorZolwia]bool{}
// 	ostatnie := []proto.KolorZolwia{}

// 	for i := len(pola) - 1; i >= 0; i-- {
// 		pole := pola[i]
// 		if len(pole.Zolwie) == 0 {
// 			continue
// 		}

// 		ostatnie = pole.Zolwie

// 		for _, zolw := range pole.Zolwie {
// 			wystapienia[zolw] = true
// 		}
// 	}

// 	if len(wystapienia) == 5 {
// 		return ostatnie
// 	}

// 	ostatnie = []proto.KolorZolwia{}
// 	for _, zolw := range zolwie {
// 		if _, ok := wystapienia[zolw]; !ok {
// 			ostatnie = append(ostatnie, zolw)
// 		}
// 	}
// 	return ostatnie

// 	// for _, pole := range pola {
// 	// 	if len(pole.Zolwie) > 0 {
// 	// 		return pole.Zolwie
// 	// 	}
// 	// }
// 	// return zolwie
// }

func ostatnieZolwie(pola []proto.Pole) []proto.KolorZolwia {
	players := 0
	for _, f := range pola {
		players += len(f.Zolwie)
	}
	if players == len(zolwie) {
		for _, f := range pola {
			if len(f.Zolwie) > 0 {
				return f.Zolwie
			}
		}
	}
	result := []proto.KolorZolwia{}
outer:
	for _, color := range zolwie {
		for _, f := range pola {
			for _, c := range f.Zolwie {
				if c == color {
					continue outer
				}
			}
		}
		result = append(result, color)
	}
	return result
}

func copyPola(pola []*proto.Pole) []proto.Pole {
	polaCopy := []proto.Pole{}
	for _, p := range pola {
		polaCopy = append(polaCopy, proto.Pole{
			Zolwie: p.Zolwie,
		})
	}
	return polaCopy
}

func pobierzDostepneKolory(pola []proto.Pole, karta proto.Karta) []proto.KolorZolwia {
	switch karta {
	case dowolnyDoPrzodu:
		return zolwie
	case dowolnyDoTylu:
		return dostepneDoCofania(pola)
	case ostatniDoPrzodu, ostatniDwaDoPrzodu:
		return ostatnieZolwie(pola)
	}

	dostepne := dostepneDoCofania(pola)
	switch karta {
	case proto.Karta_R1B:
		if slices.Contains(dostepne, proto.KolorZolwia_RED) {
			return []proto.KolorZolwia{proto.KolorZolwia_RED}
		}
		return nil
	case proto.Karta_G1B:
		if slices.Contains(dostepne, proto.KolorZolwia_GREEN) {
			return []proto.KolorZolwia{proto.KolorZolwia_GREEN}
		}
		return nil
	case proto.Karta_B1B:
		if slices.Contains(dostepne, proto.KolorZolwia_BLUE) {
			return []proto.KolorZolwia{proto.KolorZolwia_BLUE}
		}
		return nil
	case proto.Karta_Y1B:
		if slices.Contains(dostepne, proto.KolorZolwia_YELLOW) {
			return []proto.KolorZolwia{proto.KolorZolwia_YELLOW}
		}
		return nil
	case proto.Karta_P1B:
		if slices.Contains(dostepne, proto.KolorZolwia_PURPLE) {
			return []proto.KolorZolwia{proto.KolorZolwia_PURPLE}
		}
		return nil
	}

	return []proto.KolorZolwia{proto.KolorZolwia_XXX}
}

func normalizuj(liczba, od, do int) float64 {
	return float64(liczba-od) / float64(do-od)
}

func dostepneDoCofania(pola []proto.Pole) []proto.KolorZolwia {
	dostepne := []proto.KolorZolwia{}
	for _, pole := range pola {
		for _, zolw := range pole.Zolwie {
			dostepne = append(dostepne, zolw)
		}
	}
	return dostepne
}
