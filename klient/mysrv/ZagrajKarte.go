package mysrv

import (
	"gitlab.com/Dzejk0P/turtle-bot/proto"
)

type karta struct {
	ruch  int
	kolor proto.KolorZolwia
}

var ruchy = map[proto.Karta]karta{
	proto.Karta_R1:  {1, proto.KolorZolwia_RED},
	proto.Karta_R2:  {2, proto.KolorZolwia_RED},
	proto.Karta_R1B: {-1, proto.KolorZolwia_RED},
	proto.Karta_G1:  {1, proto.KolorZolwia_GREEN},
	proto.Karta_G2:  {2, proto.KolorZolwia_GREEN},
	proto.Karta_G1B: {-1, proto.KolorZolwia_GREEN},
	proto.Karta_B1:  {1, proto.KolorZolwia_BLUE},
	proto.Karta_B2:  {2, proto.KolorZolwia_BLUE},
	proto.Karta_B1B: {-1, proto.KolorZolwia_BLUE},
	proto.Karta_Y1:  {1, proto.KolorZolwia_YELLOW},
	proto.Karta_Y2:  {2, proto.KolorZolwia_YELLOW},
	proto.Karta_Y1B: {-1, proto.KolorZolwia_YELLOW},
	proto.Karta_P1:  {1, proto.KolorZolwia_PURPLE},
	proto.Karta_P2:  {2, proto.KolorZolwia_PURPLE},
	proto.Karta_P1B: {-1, proto.KolorZolwia_PURPLE},
	proto.Karta_L1:  {1, proto.KolorZolwia_XXX},
	proto.Karta_L2:  {2, proto.KolorZolwia_XXX},
	proto.Karta_A1:  {1, proto.KolorZolwia_XXX},
	proto.Karta_A1B: {-1, proto.KolorZolwia_XXX},
}

func ZagrajKarte(stan []proto.Pole, c proto.Karta, addColor proto.KolorZolwia) []proto.Pole {
	color := ruchy[c].kolor

	if color == proto.KolorZolwia_XXX {
		color = addColor
	}

	pola := movePawn(stan, color, ruchy[c].ruch)

	// endGame, _ := CheckIfGameOver(pola)
	// if endGame {
	// 	game.isEnd = true
	// 	_, pi := findWinner(game.board, game.players)
	// 	game.winer = pi
	// 	return nil
	// }

	return pola
}

func movePawn(board []proto.Pole, pawn proto.KolorZolwia, move int) []proto.Pole {

	fieldNumber, pawnNumber := findPawn(pawn, board)

	newIndex := fieldNumber + move
	if newIndex >= len(board) {
		newIndex = len(board) - 1
	}
	if fieldNumber < 0 && newIndex < 0 {
		return board
	}
	if fieldNumber == -1 || pawnNumber == -1 {
		board[newIndex].Zolwie = append(board[newIndex].Zolwie, pawn)
		return board

	}
	pawnsToSwap := board[fieldNumber].Zolwie[pawnNumber:]
	board[fieldNumber].Zolwie = board[fieldNumber].Zolwie[:pawnNumber]
	if newIndex >= 0 {
		board[newIndex].Zolwie = append(board[newIndex].Zolwie, pawnsToSwap...)
	}
	return board
}

func findPawn(pawn proto.KolorZolwia, board []proto.Pole) (fieldNumber int, pawnNumber int) {
	for fieldNumber, field := range board {
		for pawnNumber, p := range field.Zolwie {
			if p == pawn {
				return fieldNumber, pawnNumber
			}
		}
	}
	return -1, -1
}

func copyPola(pola []*proto.Pole) []*proto.Pole {
	polaCopy := []*proto.Pole{}
	for _, p := range pola {
		polaCopy = append(polaCopy, &proto.Pole{
			Zolwie: p.Zolwie,
		})
	}
	return polaCopy
}
